{-# LANGUAGE OverloadedStrings #-}
module Text.Encyclopedia.Article (
      Article(..)
    , InflectedPOS(..)
    , Inflection(..)
    , POS(..)
    , article
    , headwords_
    , inflectedPOS_
    , body_
  ) where

import Control.Applicative ((<|>), liftA2, empty, optional)
import Data.Attoparsec.Text as Atto (
      (<?>), Parser, manyTill, peekChar, satisfy, sepBy1, string, takeText
    , takeWhile, takeWhile1
  )
import Data.Attoparsec.Combinator (lookAhead)
import Data.Char (isLower, isPunctuation, isSpace, isUpper, toLower)
import Data.Text as Text (Text, append, isPrefixOf, length, map, pack, splitAt, unpack)
import Text.Printf (printf)

data POS = Adjective | Substantive | Verb deriving (Bounded, Enum, Eq, Show)
data Inflection = Masculine | Feminine deriving (Bounded, Enum, Eq, Show)

data InflectedPOS = InflectedPOS {
      pos :: POS
    , inflection :: Maybe Inflection
  } deriving (Eq, Show)

data Article = Article {
      headwords :: [String]
    , inflectedPOS :: Maybe InflectedPOS
    , body :: Text
  } deriving (Eq, Show)

(<!>) :: Parser a -> String -> Parser a
(<!>) p expected = p <|> (takeText >>= fail . formatMessage)
  where
    ellipsis s
      | Prelude.length s < 21 = s
      | otherwise = take 20 s ++ "…"
    formatMessage = printf "%s (got «%s»)" expected . ellipsis . Text.unpack
infix 0 <!>

article :: Parser Article
article = Article <$> headwords_ <*> optional inflectedPOS_ <*> body_

blank :: Parser Text
blank = Atto.takeWhile isSpace

punctOrSpace :: Parser Text
punctOrSpace = takeWhile1 (liftA2 (||) isPunctuation isSpace)

abbreviation :: (Bounded a, Enum a, Show a) => Parser a
abbreviation =
  recognize =<< takeWhile1 (not . isPunctuation) <* satisfy isPunctuation
  where
    candidates = [minBound .. maxBound]
    recognize abbr = foldr (tryOne $ Text.map toLower abbr) empty candidates
    tryOne read candidate next
      | read `isPrefixOf` (Text.map toLower . Text.pack $ show candidate) =
          pure candidate
      | otherwise = next

many1Till :: Parser a -> Parser b -> Parser [a]
many1Till p after = (:) <$> p <*> manyTill p after

headwords_ :: Parser [String]
headwords_ =
  (headword `sepBy1` headwordSeparator <!> "at least one headword") <* blank
  where
    headword =
      satisfy isUpper `many1Till` lookAhead (sentenceBegining <|> punctOrSpace)
    headwordSeparator =
      satisfy isPunctuation <* blank <* optional (string "ou") <* blank

sentenceBegining :: Parser Text
sentenceBegining = do
  upper <- satisfy isUpper
  lower <- satisfy isLower
  pure $ Text.pack [upper, lower]

inflectedPOS_ :: Parser InflectedPOS
inflectedPOS_ = InflectedPOS
  <$> (punctOrSpace *> abbreviation <* blank)
  <*> (optional abbreviation <* blank)
  <!> "inflectedPOS"

body_ :: Parser Text
body_ = Text.append <$> sentenceBegining <*> takeText <!> "body"
