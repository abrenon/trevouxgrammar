{-# LANGUAGE NamedFieldPuns #-}
module Main where

import Data.Attoparsec.Text (parseOnly)
import Data.List (intercalate)
import Data.Text as Text (append, pack)
import Data.Text.IO as Text (readFile, writeFile)
import System.Directory (createDirectoryIfMissing)
import System.Environment (getArgs, getProgName)
import System.Exit (die)
import System.FilePath ((</>), dropExtension, takeDirectory, takeFileName)
import System.IO (Handle, IOMode(..), hClose, hPutStrLn, openFile, stderr)
import Text.Encyclopedia.Article (Article(..), InflectedPOS(..), article)
import Text.Printf (printf)

data OutputContext = OutputContext {
      files :: Handle
    , meta :: Handle
    , targetRoot :: FilePath
  }

source :: String
source = "ARTFL"

process :: OutputContext -> FilePath -> IO ()
process context filepath =
  logOrOutput . parseOnly article =<< Text.readFile filepath
  where
    logOrOutput (Left why) = hPutStrLn stderr $ printf "%s: %s" filepath why
    logOrOutput (Right parsed) = outputData context filepath parsed

outputData :: OutputContext -> FilePath -> Article -> IO ()
outputData (OutputContext {files, meta, targetRoot})
           filepath
           (Article {headwords, inflectedPOS, body}) = do
    hPutStrLn files $ csv [textId, fileDir, fileName]
    hPutStrLn meta $ csv [textId, source, renderHW, renderPOS, renderFeature]
    createDirectoryIfMissing True $ destPath
    Text.writeFile (destPath </> fileName) body
  where
    fileDir = takeFileName $ takeDirectory filepath
    fileName = takeFileName filepath
    destPath = targetRoot </> fileDir
    textId = fileDir ++ dropExtension fileName
    csv = intercalate ","
    renderHW = intercalate ":" ("":headwords ++ [""])
    (renderPOS, renderFeature) = maybe ("", "") extractPOS inflectedPOS
    extractPOS (InflectedPOS {pos, inflection}) =
      (show pos, maybe "" show inflection)

populate :: FilePath -> IO ()
populate targetRoot = do
  createDirectoryIfMissing True $ targetRoot
  files <- openFile (targetRoot </> "files.csv") WriteMode
  meta <- openFile (targetRoot </> "meta.csv") WriteMode
  hPutStrLn files "id,path,file"
  hPutStrLn meta "id,source,headword,POS,features"
  let context = OutputContext {files, meta, targetRoot}
  getContents >>= mapM_ (process context) . lines

main :: IO ()
main = getArgs >>= handle
  where
    handle [targetDirectory] = populate targetDirectory
    handle _ = getProgName >>= die . syntax
    syntax =
      printf "Syntax: %s TARGET_DIRECTORY (input files get read from stdin"
