{-# LANGUAGE OverloadedStrings #-}
module ExtractMetadata (
    tests
  ) where

import Data.Attoparsec.Text (Parser, parseOnly)
import Data.Text as Text (Text, pack)
import Distribution.TestSuite (Test, testGroup)
import Text.Encyclopedia.Article (
    InflectedPOS(..), Inflection(..), POS(..), headwords_, inflectedPOS_, body_
  )
import Toolbox (simpleTest)
import Text.Printf (printf)

checkAttoparsec :: (Show a, Eq a) => Parser a -> (String, Text, Either String a) -> Test
checkAttoparsec parser (what, input, expected) =
  simpleTest (what, parseOnly parser input, expected)

errorMessage :: String -> String -> Either String a
errorMessage expected =
  Left . printf "Failed reading: %s (got «%s»)" expected . ellipsis
  where
    ellipsis s
      | length s < 21 = s
      | otherwise = take 20 s ++ "…"

testHeadwordGrammar :: Test
testHeadwordGrammar = testGroup "headword" $ checkAttoparsec headwords_ <$> [
      ("simple headword", "TEST.", Right ["TEST"])
    , ("2 headwords", "KHEIR, ou KEIR.", Right ["KHEIR", "KEIR"])
    , ("several headwords", "HUEY, DEWEY, ou LOUIE.", Right ["HUEY", "DEWEY", "LOUIE"])
    , ("not a head", "truc", errorMessage "at least one headword" "truc")
  ]


testPOSGrammar :: Test
testPOSGrammar = testGroup "POS" [
      testGroup "POS-only" $ checkAttoparsec inflectedPOS_ <$> [
            ("substantive", " s.", Right subst)
          , ("verb", " v.", Right verb)
          , ("bad POS", "truc", errorMessage "inflectedPOS" "truc")
        ]
    , testGroup "inflected" $ checkAttoparsec inflectedPOS_ <$> [
            ("fem. subst.", " s. f.", Right substF)
          , ("masc. adj.", " adj. m.", Right adjM)
        ]
  ]
  where
    subst = InflectedPOS Substantive Nothing
    verb = InflectedPOS Verb Nothing
    substF = subst {inflection = Just Feminine}
    adjM = InflectedPOS Adjective (Just Masculine)

testBodyGrammar :: Test
testBodyGrammar = testGroup "body" $ checkAttoparsec body_ <$> [
      ("Positive", positive, Right positive)
    , ("Negative", Text.pack negative, errorMessage "body" negative)
  ]
  where
    positive = "Une phrase commence par une majuscule."
    negative = "pas par une minuscule"

tests :: IO [Test]
tests = pure [
      testHeadwordGrammar
    , testPOSGrammar
    , testBodyGrammar
  ]
