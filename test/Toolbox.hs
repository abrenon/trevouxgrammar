{-# LANGUAGE NamedFieldPuns #-}
module Toolbox (
      SimpleTest
    , simpleTest
  ) where

import Distribution.TestSuite (
    Progress(..), Result(..), Test(..), TestInstance(..)
  )
import Text.Printf (printf)

type SimpleTest a = (String, a, a)

simpleTest :: (Show a, Eq a) => SimpleTest a -> Test
simpleTest (name, actual, expected) = Test $ TestInstance {
      run = check name actual expected
    , name
    , tags = []
    , options = []
    , setOption = \_ _ -> Left "No option available in this simple test"
  }

check :: (Show a, Eq a) => String -> a -> a -> IO Progress
check name actual expected
  | actual == expected = pure . Finished $ Pass
  | otherwise = do
    printf "Expected %s but got %s\n" (show expected) (show actual)
    pure . Finished $ Fail name
